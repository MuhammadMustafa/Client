const CRAWLER_BASE_URL = import.meta.env.VITE_API_CRAWLER_URL;

export const fetchCrawlerData = async () => {
  const response = await fetch(`${CRAWLER_BASE_URL}`);
  return response.json();
};

export async function postCrawler(urls) {
  const response = await fetch(`${CRAWLER_BASE_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(urls),
  });
  return await response.json();
}
export async function deleteCrawler(url) {
  const response = await fetch(`${CRAWLER_BASE_URL}/${url}`, {
    method: "DELETE",
  });
  return await response.json();
}
