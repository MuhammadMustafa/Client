import { useEffect, useState } from "react";
import ServiceComponent from "../Components/ServiceComponent";

export default function RunningServices() {
  const [services, setServices] = useState([
    {
      id: "b957a42266e1",
      image: "image-0.3898579002140954",
      containerName: "container-0.3898579002140954",
      port: "0.0.0.0:8085->8000/tcp",
    },
    {
      id: "b957a42266e2",
      image: "image-0.3898579002140954",
      containerName: "container-0.3898579002140954",
      port: "0.0.0.0:8085->8000/tcp",
    },
    {
      id: "b957a42266e3",
      image: "image-0.3898579002140954",
      containerName: "container-0.3898579002140954",
      port: "0.0.0.0:8085->8000/tcp",
    },
  ]);

  useEffect(() => {
    console.log(services);
  }, []);

  return (
    <>
      <div className="flex items-center">
        <h1 className="p-5 h-full text-xl font-bold">All Running Service</h1>
      </div>

      {services.map(service => {
        return <ServiceComponent key={service.id} service={service} />;
      })}
    </>
  );
}
