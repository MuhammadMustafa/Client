import { useState, useEffect } from "react";
import CrawlerListComponent from "../Components/CrawlerListComponent";
import { Link } from "react-router-dom";
import { IoMdAddCircleOutline } from "react-icons/io";

export default function CrawlerPage() {
  const [urls, seturls] = useState(["https://www.google.com", "https://www.facebook.com", "https://www.twitter.com"]);

  useEffect(() => {
    console.log(urls);
  }, []);

  return (
    <>
      <div className="flex items-center">
        <h1 className="p-5 h-full text-xl font-bold">Crawlers Url</h1>
        <Link to={"new"} className="ml-auto bg-gray-900 text-white p-2  rounded  flex items-center mr-4">
          <IoMdAddCircleOutline className="size-5 text-white mx-1" />
        </Link>
      </div>

      {urls.map(url => {
        return <CrawlerListComponent key={url} url={url} />;
      })}
    </>
  );
}
