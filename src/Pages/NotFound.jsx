import { FaSpider } from "react-icons/fa";
import { IoConstruct } from "react-icons/io5";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="flex flex-col justify-center items-center h-screen">
      <div className="flex">
        <FaSpider className="size-12 text-gray-900 mx-1" />
        <IoConstruct className="size-12 text-gray-900 mx-1" />
      </div>
      <h1 className="text-4xl font-bold text-gray-900">Not Found or Under Construction</h1>
      <Link to={"/"} className="bg-gray-900 text-white px-12 py-2 font-bold rounded  flex items-center mt-4">
        Go Home
      </Link>
    </div>
  );
};

export default NotFound;
