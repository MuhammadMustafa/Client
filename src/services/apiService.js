const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;

export const fetchApiData = async () => {
  console.log(import.meta.env.VITE_API_BASE_URL);
  const response = await fetch(`${API_BASE_URL}/apis`);
  return response.json();
};
export function updateApi(id, api) {
  const response = fetch(`${API_BASE_URL}/apis/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(api),
  });
  return response.json();
}

export async function postAPI(api) {
  console.log(api);
  const response = await fetch(`${API_BASE_URL}/apis`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(api),
  });
  return await response.json();
}
