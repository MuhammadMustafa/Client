const CrawlerListComponent = ({ url }) => {
  return (
    <div className="bg-white p-4 mb-4 rounded flex mx-4 shadow items-end border-t-4 border-solid border-green-400">
      <div className="flex flex-col items-start">
        <a href={url} className="text-sm text-blue-600 mb-2">
          {url}
        </a>
      </div>
    </div>
  );
};

export default CrawlerListComponent;
