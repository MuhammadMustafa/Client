import { useState } from "react";
import { updateApi } from "../Services/apiService";

const ApiItem = ({ api }) => {
  const [status, setStatus] = useState(api.status);

  const handleStatusChange = async status => {
    api.status = status;
    updateApi(api.id, api);

    console.log("Changing status for:", api.name, " to:", status);
  };

  const [editMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    console.log("Toggling edit mode", editMode);
    if (editMode) {
      setEditMode(false);
      handleStatusChange(status).then(() => console.log("Status changed"));
    } else {
      setEditMode(true);
    }
  };

  function borderColorByStatus(status) {
    switch (status) {
      case "NEW":
        return " border-blue-400";
      case "FREE":
        return " border-green-400";
      case "UNCERTAIN":
        return " border-yellow-400";
      case "UNAVAILABLE":
        return " border-red-400";
      default:
        return " border-gray-400";
    }
  }

  return (
    <div
      className={
        "bg-white p-4 mb-4 rounded flex mx-4 shadow items-end border-t-4 border-solid" + borderColorByStatus(api.status)
      }>
      <div className="flex flex-col items-start">
        <p className="text-lg font-semibold mb-2">
          {api.name} - {api.status}
        </p>
        <a href={api.endpoint} className="text-sm text-blue-600 mb-2">
          {api.endpoint}
        </a>
        <p className="text-sm text-gray-600">description: {api.description}</p>
      </div>

      {editMode && (
        <div className="ml-auto">
          <select
            onChange={e => setStatus(e.target.value)}
            value={status}
            className="bg-white border border-gray-400 rounded px-4 py-2">
            <option value="NEW">NEW</option>
            <option value="FREE">FREE</option>
            <option value="UNCERTAIN">UNCERTAIN</option>
            <option value="UNAVAILABLE">UNAVAILABLE</option>
          </select>
        </div>
      )}

      <button
        onClick={toggleEditMode}
        // add ml-auto if editMode is false
        className={
          editMode ? "p-2 ml-4 bg-green-600 text-white px-4 rounded" : "p-2 ml-auto bg-blue-600 text-white px-4 rounded"
        }>
        {editMode ? "Save" : "Edit"}
      </button>
    </div>
  );
};

export default ApiItem;
