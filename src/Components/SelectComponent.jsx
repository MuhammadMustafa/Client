export default function SelectComponent({ id, name, handleChange, statusOptions }) {
  return (
    <>
      <label htmlFor="APIStatus" className="block my-2 text-sm font-medium text-gray-900 ">
        API status
      </label>
      <select
        id={id}
        name={name}
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
        onChange={handleChange}
        required>
        {statusOptions.map(status => (
          <option key={status} value={status}>
            {status}
          </option>
        ))}
      </select>
    </>
  );
}
