export default function Button({ text, type, classList }) {
  return (
    <div className="mt-4 flex">
      <button className={classList} type={type}>
        {text}
      </button>
    </div>
  );
}
