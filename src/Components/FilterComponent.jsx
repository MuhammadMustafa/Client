export default function FilterComponent({ filter, setFilter }) {
  return (
    <select
      className="bg-gray-900 text-white mx-2 border border-gray-400 rounded px-4 py-2"
      onChange={e => setFilter(e.target.value)}
      value={filter}>
      <option value="ALL">ALL</option>
      <option value="NEW">NEW</option>
      <option value="FREE">FREE</option>
      <option value="UNCERTAIN">UNCERTAIN</option>
      <option value="UNAVAILABLE">UNAVAILABLE</option>
    </select>
  );
}
