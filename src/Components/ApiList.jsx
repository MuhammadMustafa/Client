// src/components/ApiList.js
import ApiItem from "./ApiItem";
import { IoMdAddCircleOutline } from "react-icons/io";
import { Link } from "react-router-dom";
// import { useApiContext } from "../context/ApiContext";
import { useEffect, useState } from "react";
import FilterComponent from "./FilterComponent";
import { fetchApiData } from "../Services/apiService";

const ApiList = () => {
  const [apiList, setApiList] = useState([]);
  let [filter, setFilter] = useState("ALL");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchApiData();
        setApiList(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="flex items-center">
        <h1 className="p-5 h-full text-xl font-bold">All System APIs</h1>

        <div className="flex ml-auto">
          <FilterComponent filter={filter} setFilter={setFilter} />

          <Link to={"/add"} className="bg-gray-900 text-white p-2  rounded  flex items-center mr-4">
            <IoMdAddCircleOutline className="size-5 text-white mx-1" />
          </Link>
        </div>
      </div>

      {filter === "ALL"
        ? apiList.map(api => <ApiItem key={api.id} api={api} />)
        : apiList.filter(api => api.status === filter).map(api => <ApiItem key={api.id} api={api} />)}
    </>
  );
};

export default ApiList;
