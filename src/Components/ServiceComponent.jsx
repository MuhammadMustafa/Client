import { useEffect } from "react";

const ServiceComponent = ({ service }) => {
  useEffect(() => {
    console.log(service);
  }, [service]);

  return (
    <div className="bg-white p-4 mb-4 rounded flex mx-4 shadow items-end border-t-4 border-solid border-green-400">
      <div className="flex flex-col items-start">
        <p className="text-lg font-semibold mb-2">{service.id}</p>
        <p className="text-lg font-semibold mb-2">
          {service.image} - {service.containerName}
        </p>
        <a href={service.port} className="text-sm text-blue-600 mb-2">
          {service.port}
        </a>
      </div>
    </div>
  );
};

export default ServiceComponent;
